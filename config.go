package main

import (
	"fmt"
	"os"

	"github.com/BurntSushi/toml"
)

type Configuration struct {
	Channels []string
	DLPath   string
	LDBPath  string
}

func getConfig() Configuration {
	var cfg Configuration

	cfgPath := os.Getenv("TVR_CONFIG_PATH")
	if len(cfgPath) == 0 {
		fmt.Println("Couldn't find a specified configuration file, proceeding with default.")
		cfgPath = "config.toml"
	}
	data, err := os.ReadFile(cfgPath)
	if err != nil {
		panic(err)
	}

	_, err = toml.Decode(string(data), &cfg)
	if err != nil {
		panic(err)
	}

	return cfg
}
