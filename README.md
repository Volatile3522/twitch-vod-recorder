### The Twitch VOD Recorder

A very simple Twitch VOD recorder using the TwitchRSS feed.

```shell
go build -ldflags "-s -w" -o recorder *.go
cp config.toml.example config.toml
$EDITOR config.toml
./recorder
```

#### TODO

- [ ] Dockerize this