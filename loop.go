package main

import (
	"fmt"
	"os"
	"os/exec"
	"path"
	"strconv"
	"time"

	"github.com/mmcdole/gofeed"
)

func (r recorder) downloadStream(channel string, item *gofeed.Item) {
	timeStr := item.PublishedParsed.Month().String() + " " + strconv.Itoa(item.PublishedParsed.Day())

	dl := exec.Command("yt-dlp", item.Link)
	dl.Dir = path.Join(r.config.DLPath, channel, strconv.Itoa(item.PublishedParsed.Year()), timeStr)

	err := os.MkdirAll(dl.Dir, 0777)
	fatalIfReal(err)

	dl.Stdout = os.Stdout
	dl.Stderr = os.Stderr
	err = dl.Run()
	fatalIfReal(err)
}

func (r recorder) loop() {
	for {
		for _, channel := range r.config.Channels {
			var newSeen uint

			fp := gofeed.NewParser()
			feed, _ := fp.ParseURL("https://twitchrss.appspot.com/vodonly/" + channel)

			/* j, _ := json.MarshalIndent(feed, "  ", "  ")
			err := os.WriteFile("feed.json", j, 0777)
			fatalIfReal(err)*/

			for _, item := range feed.Items {
				guidBytes := []byte(item.GUID)

				seen, err := r.db.Has(guidBytes, nil)
				fatalIfReal(err)

				if seen {
					continue
				}

				err = r.db.Put(guidBytes, []byte{0xFF}, nil)
				fatalIfReal(err)

				go r.downloadStream(channel, item)
				newSeen++
			}

			fmt.Printf("Scraped %s at %s. Got %d new items, %d in total.\n", channel, time.Now().String(), newSeen, len(feed.Items))
		}
		time.Sleep(30 * time.Minute)
	}
}
