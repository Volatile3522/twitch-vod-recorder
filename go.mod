module codeberg.org/Volatile3522/twitch-vod-recorder

go 1.19

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/mmcdole/gofeed v1.1.3
	github.com/syndtr/goleveldb v1.0.0
)

require (
	github.com/PuerkitoBio/goquery v1.5.1 // indirect
	github.com/andybalholm/cascadia v1.1.0 // indirect
	github.com/golang/snappy v0.0.0-20180518054509-2e65f85255db // indirect
	github.com/json-iterator/go v1.1.10 // indirect
	github.com/mmcdole/goxpp v0.0.0-20181012175147-0068e33feabf // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v0.0.0-20180701023420-4b7aa43c6742 // indirect
	golang.org/x/net v0.0.0-20200301022130-244492dfa37a // indirect
	golang.org/x/text v0.3.2 // indirect
)
