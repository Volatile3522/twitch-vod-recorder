package main

import (
	"github.com/syndtr/goleveldb/leveldb"
)

type recorder struct {
	config Configuration
	db     *leveldb.DB
}

func fatalIfReal(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	var rec recorder

	rec.config = getConfig()

	database, err := leveldb.OpenFile(rec.config.LDBPath, nil)
	fatalIfReal(err)
	rec.db = database

	rec.loop()
}
